// Karma configuration
// Generated on Thu Jul 07 2016 17:30:07 GMT+0300 (EEST)
const webpackConfig = require('./utils/webpack/testConfig');

module.exports = function makeConfig(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    client: {
      chai: {
        includeStack: true,
      },
      captureConsole: false,
    },

    frameworks: ['mocha', 'sinon-chai', 'es6-shim'],

    reporters: ['mocha', 'coverage', 'browser'],

    files: [
      './tests/test_index.js',
    ],

    exclude: [
      './src/js/index.js',
    ],


    preprocessors: {
      './src/**/*.js': ['webpack', 'sourcemap'],
      './tests/test_index.js': ['webpack', 'sourcemap'],
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      noInfo: true,
    },

    coverageReporter: {
      dir: 'coverage',
      subdir: '.',
      includeAllSources: true,
      check: {
        global: {
          statements: 100,
          branches: 100,
          functions: 100,
          lines: 100,
        },
      },
    },

    browserReporter: {
      port: 5432,
      ignoreSuccessful: false,
      ignoreFailed: false,
      ignoreSkipped: false,
    },

    plugins: [
      'karma-webpack',
      'karma-mocha',
      'karma-sinon-chai',
      'karma-es6-shim',
      'karma-phantomjs-launcher',
      'karma-chrome-launcher',
      'karma-coverage',
      'karma-mocha-reporter',
      'karma-browser-reporter',
      'karma-sourcemap-loader',
    ],

    port: 9876,
    colors: true,
    logLevel: config.LOG_WARN,
    autoWatch: true,
    browsers: ['PhantomJS'],
    browserDisconnectTimeout: 10000, // default 2000
    browserDisconnectTolerance: 1, // default 0
    browserNoActivityTimeout: 60000, // default 10000
    singleRun: false,
    concurrency: Infinity,
  });
};
