const express = require('express');
const app = express();

const HTML_PATH = `${__dirname}/dist/index.html`;

const PORT = 8080;

if (process.env.NODE_ENV === 'development') {
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const webpackConfig = require('./utils/webpack/devConfig');
  const compiler = webpack(webpackConfig);
  const devMiddlewareConfig = {
    noInfo: true,
    hot: true,
    inline: true,
  };
  app.use(webpackDevMiddleware(compiler, devMiddlewareConfig));
  app.use(webpackHotMiddleware(compiler));
}

app.use(express.static('dist'));
app.get('*', (req, res) => {
  res.sendFile(HTML_PATH);
});

const callback = () => console.log(`Example app listening on port ${PORT}!`);

app.listen(PORT, callback);
