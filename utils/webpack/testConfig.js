const path = require('path');
const webpack = require('webpack');
const defaultConfig = require('./defaultConfig');

const testConfig = Object.assign({}, defaultConfig);

testConfig.devtool = 'inline-source-map';

testConfig.node = {
  fs: 'empty',
};

testConfig.externals = {
  'react/addons': true,
  'react/lib/ExecutionEnvironment': true,
  'react/lib/ReactContext': true,
};

module.exports = testConfig;
