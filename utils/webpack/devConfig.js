const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExportFilesWebpackPlugin = require('export-files-webpack-plugin');
const defaultConfig = require('./defaultConfig');

const ROOT_PATH = path.join(__dirname, '../../');

const devConfig = Object.assign({}, defaultConfig);

devConfig.devtool = 'eval-source-map';

devConfig.entry = [
  'react-hot-loader/patch',
  'webpack-hot-middleware/client',
  defaultConfig.entry,
];

devConfig.plugins = devConfig.plugins.concat([
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoErrorsPlugin(),
  new HtmlWebpackPlugin({
    template: 'src/templates/index.html',
    inject: true,
  }),
  new ExportFilesWebpackPlugin('index.html', {
    outputPath: path.join(ROOT_PATH, 'dist'),
  }),
]);

devConfig.resolve.alias = {
  // is needed for item-builders to use the same React instance as in assessmnet builder;
  react$: path.join(__dirname, '/../../node_modules/react/react.js'),
};

module.exports = devConfig;
