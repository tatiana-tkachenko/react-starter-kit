const path = require('path');
const webpack = require('webpack');
const defaultConfig = require('./defaultConfig');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const prodConfig = Object.assign({}, defaultConfig);

prodConfig.output.path = path.join(__dirname, '../../build/dist');
prodConfig.output.filename = '[hash].js';

prodConfig.devtool = 'source-map';
prodConfig.plugins = prodConfig.plugins.concat([
  new webpack.optimize.OccurenceOrderPlugin(true),
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.UglifyJsPlugin(),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production'),
    },
  }),
  new HtmlWebpackPlugin({
    template: 'src/templates/index.html',
    inject: true,
  }),
]);

prodConfig.resolve.alias = {
  // is needed for item-builders to use the same React instance as in assessmnet builder;
  react$: path.join(__dirname, '/../../node_modules/react/react.js'),
};

module.exports = prodConfig;
