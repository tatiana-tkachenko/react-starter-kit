import React from 'react';
import { mount } from 'enzyme';
import Counter from '../src/js/Counter.jsx';

let wrapper;
let counterValue;

beforeEach(() => {
  counterValue = 10;
  wrapper = mount(<Counter currentValue={counterValue} />);
});

describe('<Counter />', () => {
  it('renders <Counter /> component', () => {
    expect(wrapper).to.have.length(1);
  });

  it('renders input field within component', () => {
    expect(wrapper.find('.counter')).to.have.length(1);
  });

  it('display default value in counter', () => {
    const value = wrapper.find('.counter').text();
    expect(value).to.eql('10');
  });
});
