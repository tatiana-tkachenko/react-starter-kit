import React from 'react';
import { mount } from 'enzyme';
import Button from '../src/js/Button.jsx';

let wrapper;
let onClickSpy;
let buttonType;

beforeEach(() => {
  buttonType = 'increase';
  onClickSpy = sinon.spy();
  wrapper = mount(<Button click={onClickSpy} type={buttonType} />);
});

describe('<Button />', () => {
  it('renders <Button /> component', () => {
    expect(wrapper).to.have.length(1);
  });

  it('renders one button within component', () => {
    expect(wrapper.find('button')).to.have.length(1);
  });

  it('displays "+" in button when "type" prop is set to "increase"', () => {
    const buttonNode = wrapper.find('button');
    expect(buttonNode.text()).to.eql('+');
  });

  it('displays "-" in button when "type" prop is set to "decrease"', () => {
    wrapper.setProps({ type: 'decrease' });
    const buttonNode = wrapper.find('button');
    expect(buttonNode.text()).to.eql('-');
  });

  it('calls "onClick" prop when clicked', () => {
    wrapper.find('button').simulate('click');
    expect(onClickSpy).to.have.been.calledOnce;
  });
});
