import React from 'react';
import { mount } from 'enzyme';
import InputField from '../src/js/InputField.jsx';

let wrapper;
let onChangeSpy;
let incrementValue;

beforeEach(() => {
  incrementValue = 1;
  onChangeSpy = sinon.spy();
  wrapper = mount(<InputField change={onChangeSpy} currentValue={incrementValue} />);
});

describe('<InputField />', () => {
  it('renders <InputField /> component', () => {
    expect(wrapper).to.have.length(1);
  });

  it('renders counter field within component', () => {
    expect(wrapper.find('input')).to.have.length(1);
  });

  it('display default value in input field', () => {
    const value = wrapper.find('input').prop('value');
    expect(value).to.eql(1);
  });
  it('calls "onCange" prop when input new value', () => {
    wrapper.find('input').simulate('change');
    expect(onChangeSpy).to.have.been.calledOnce;
  });
  it('input value shoud be a number', () => {
    wrapper.find('input').simulate('change', { target: { value: 'one' } });
    expect(onChangeSpy.withArgs(0)).to.have.been.calledOnce;
  });
  it('props.change called with correct arguments', () => {
    const value = 2;
    wrapper.find('input').simulate('change', { target: { value } });
    expect(onChangeSpy.withArgs(value)).to.have.been.calledOnce;
  });
});
