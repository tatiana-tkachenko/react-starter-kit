import React, { Component } from 'react';
import Button from './Button.jsx';
import InputField from './InputField.jsx';
import Counter from './Counter.jsx';

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      currentCounterValue: 10,
      incrementValue: 1,
      interval: undefined,
    };

    this.incrementCounterValue = this.incrementCounterValue.bind(this);
    this.onClickDecrementHandler = this.onClickDecrementHandler.bind(this);
    this.onChangeHundler = this.onChangeHundler.bind(this);
    this.onClickIncrementHandler = this.onClickIncrementHandler.bind(this);
  }

  componentWillMount() {
    const interval = setInterval(this.incrementCounterValue, 1000);
    this.setState({ interval });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  onClickDecrementHandler() {
    const { incrementValue } = this.state;
    this.setState({ incrementValue: incrementValue - 1 });
  }

  onChangeHundler(value) {
    this.setState({ incrementValue: value });
  }

  onClickIncrementHandler() {
    const { incrementValue } = this.state;
    this.setState({ incrementValue: incrementValue + 1 });
  }

  incrementCounterValue() {
    const { currentCounterValue, incrementValue } = this.state;
    this.setState({
      currentCounterValue: currentCounterValue + incrementValue,
    });
  }

  render() {
    return (
      <div>
        <Button type="decrease" click={this.onClickDecrementHandler} />
        <InputField currentValue={this.state.incrementValue} change={this.onChangeHundler} />
        <Button type="increase" click={this.onClickIncrementHandler} />
        <Counter currentValue={this.state.currentCounterValue} />
      </div>
    );
  }
}
