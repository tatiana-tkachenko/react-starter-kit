import React, { Component } from 'react';

export default class InputField extends Component {
  constructor() {
    super();
    this.onChangeHandler = this.onChangeHandler.bind(this);
  }
  onChangeHandler(e) {
    const value = e.target.value;
    const inputValue = isNaN(value) ? 0 : +value;
    this.props.change(inputValue);
  }
  render() {
    return (
      <input
        className="input-field"
        type="text"
        value={this.props.currentValue}
        onChange={this.onChangeHandler}
      />
    );
  }
}
